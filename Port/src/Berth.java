public class Berth {
    private Ship ship;

    public synchronized void dockShip(Ship ship) {
        this.ship = ship;
        System.out.println("Ship " + ship.getName() + " docked at the berth.");
    }

    public synchronized void undockShip() {
        System.out.println("Ship " + ship.getName() + " undocked from the berth.");
        ship = null;
    }

    public Ship getShip() {
        return ship;
    }
}