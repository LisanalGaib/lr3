import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Ship extends Thread {
    private final int capacity;
    private final AtomicInteger containers;
    private final Port port;
    private Berth berth;

    public Ship(int capacity, Port port) {
        this.capacity = capacity;
        this.containers = new AtomicInteger(0);
        this.port = port;
    }

    @Override
    public void run() {
        try {
            berth = port.getBerth();
            if (berth != null) {
                berth.dockShip(this);
                loadUnload();
                berth.undockShip();
            } else {
                System.out.println("No available berth for ship " + getName());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void loadUnload() throws InterruptedException {
        Semaphore semaphore = new Semaphore(1);
        semaphore.acquire();
        try {
            while (containers.get() < capacity) {
                port.addContainer();
                containers.incrementAndGet();
                System.out.println("Ship " + getName() + " loaded a container. Total containers in port: " + port.getTotalContainers());
            }
            while (containers.get() > 0) {
                port.removeContainer();
                containers.decrementAndGet();
                System.out.println("Ship " + getName() + " unloaded a container. Total containers in port: " + port.getTotalContainers());
            }
        } finally {
            semaphore.release();
        }
    }
}