public class PortApplication {
    public static void main(String[] args) {
        Port port = new Port(100, 5);
        for (int i = 0; i < 10; i++) {
            new Ship(10, port).start();
        }
        new PortOperator(port, 5).start();
    }
}