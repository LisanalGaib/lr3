public class PortOperator extends Thread {
    private final Port port;
    private final int timeout;

    public PortOperator(Port port, int timeout) {
        this.port = port;
        this.timeout = timeout;
    }

    @Override
    public void run() {
        int iterations = 0;
        while (iterations < timeout) {
            Berth berth = port.getBerth();
            if (berth != null && berth.getShip() != null) {
                System.out.println("Port operator is working on ship " + berth.getShip().getName());
            } else {
                System.out.println("Port operator is waiting for a ship to arrive.");
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            iterations++;
        }
        System.out.println("Port operator has finished its work.");
    }
}