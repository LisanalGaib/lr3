import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Port {
    private final int capacity;
    private final int numBerths;
    private final List<Berth> berths;
    private final AtomicInteger totalContainers;

    public Port(int capacity, int numBerths) {
        this.capacity = capacity;
        this.numBerths = numBerths;
        this.berths = new ArrayList<>(numBerths);
        this.totalContainers = new AtomicInteger(0);

        for (int i = 0; i < numBerths; i++) {
            berths.add(new Berth());
        }
    }

    public Berth getBerth() {
        for (Berth berth : berths) {
            if (berth.getShip() == null) {
                return berth;
            }
        }
        return null;
    }

    public void addContainer() {
        totalContainers.incrementAndGet();
    }

    public void removeContainer() {
        totalContainers.decrementAndGet();
    }

    public int getTotalContainers() {
        return totalContainers.get();
    }

    public int getCapacity() {
        return capacity;
    }
}